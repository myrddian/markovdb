MarkovDB
==

Introduction
--

This is a Graph based database with a difference, in that it's conceptually built and uses Markov Chains
as the main graph. 

This means that the database can be used to infer values probabilistically as well as generate samples based
on the data provided.


Status
--

At the moment the program is still being built and its not even in Alpha, you are welcome to try and use it as a library
at this point.


Plan
--
 
The plan is to make the core work, then have the database communicate via a REST or JMS end points.


Concepts
--

MarkovVariable - it is an independent variable that is represented by a markov chain. 
For example VAR(WEATHER) = (SUNNY, RAINY)  - The variable weather consists of two values Sunny, Rainy
this variable has a set of edges that represent the probability of transition between the values.

MarkovObject - The main DB object, it has two parts, Fields and MarkovVariables. Fields are values that are not represented by a markov property
where as MarkovVariables are.The database only really works with the MarkovVariables not Fields, so a MarkovObject can be represented as
P(X,Y,Z| x) <- the value of a markov object is a grouping of independent MarkovVariables

